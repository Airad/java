import java.util.ArrayList;

public class Stos {

    private ArrayList<String>stos;

    public Stos(){
        stos=new ArrayList<>();
    }

    public String push(String word){
        stos.add(word);
        return word;
    }
    public String pop(){
        if(stos.size()==0) return "Stos jest pusty";
        String popword = stos.get(stos.size()-1);
        stos.remove(stos.size()-1);
        return popword;
    }
    public boolean isEmpty(){
        if(stos.size()==0) return true;
        else return false;
    }
}
