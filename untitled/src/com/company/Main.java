package com.company;

public class Main {

    public static void main(String[] args) {
	    Punkt punkt1 = new Punkt();
	    Punkt punkt2= new Punkt(12,1);
	    Punkt punkt3= new Punkt(2);

	    System.out.println("Punkt 1: \nx: "+punkt1.getX()+" y: "+punkt1.getY());
        System.out.println("Punkt 2: \nx: "+punkt2.getX()+" y: "+punkt2.getY());
        System.out.println("Punkt 3: \nx: "+punkt3.getX()+" y: "+punkt3.getY());
    }
}
