package com.company;

public class Punkt {

    private double x;
    private double y;

    public Punkt(){
        this.x=0;
        this.y=0;
    }

    public Punkt(double a, double b){
        this.x=a;
        this.y=b;
    }

    public Punkt(double a){
        this.x=a;
        this.y=0;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
